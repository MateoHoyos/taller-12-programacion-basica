import math

def simulador(Vo,a):
    lista_resultados=[]
    
    g=9.8
    y=0
    t=0
    
    while y>=0:
        x=Vo*math.cos(a)*t
        y=Vo*math.sin(a)*t -(1/2)*g*t**2
        t=t+1
        lista_resultados.append([x,y])
    return lista_resultados
r=simulador(50,math.pi/8)


for posicion in r:
    
    print("x: ", posicion[0], "y: ", posicion[1])


