
def simular_semiparabolico(vo,yo):
    lista_resultados=[]
    
    g=-9.8
    t=0
    y=0
    
    while y>=0:
        x=vo*t
        y=yo+(1/2)*g*(t**2)
        t=t+0.2
        lista_resultados.append((x,y))
    return lista_resultados
r=simular_semiparabolico(36,9)

for posicion in r:
    
    print("x: ", posicion[0], "y: ", posicion[1])
    

